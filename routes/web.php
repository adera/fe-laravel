<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});


*/
Auth::routes();
Route::get('/', 'App\Http\Controllers\HomeController@index');
Route::get('/home','App\Http\Controllers\HomeController@index')->name('home');

Route::get('/detail_jawara','App\Http\Controllers\HomeController@detail_jawara')->name('detail_jawara');
Route::get('/kota','App\Http\Controllers\HomeController@kota')->name('kota');
Route::get('/kecamatan','App\Http\Controllers\HomeController@kecamatan')->name('kecamatan');
Route::get('/kelurahan','App\Http\Controllers\HomeController@kelurahan')->name('kelurahan');

Route::get('/view_login','App\Http\Controllers\AuthController@view_login')->name('view_login');

Route::post('/login1','App\Http\Controllers\AuthController@login1')->name('login1');
Route::get('/logout','App\Http\Controllers\AuthController@logout')->name('logout');

Route::get('/register-loca','App\Http\Controllers\RegisterController@registerloca')->name('register-loca');

Route::get('/detail-loca','App\Http\Controllers\RegisterController@detailloca')->name('detail-loca');

Route::post('/simpan-loca','App\Http\Controllers\RegisterController@simpanloca')->name('simpan-loca');

Route::get('/register-jawara','App\Http\Controllers\RegisterController@registerjawara')->name('register-jawara');
Route::get('/detail-jawara','App\Http\Controllers\RegisterController@detailjawara')->name('detail-jawara');
Route::post('/simpan-jawara','App\Http\Controllers\RegisterController@simpanjawara')->name('simpan-jawara');



Route::get('/mitra-loca-umkm','App\Http\Controllers\RegisterController@mitralocaumkm')->name('mitra-loca-umkm');

Route::get('/mitra-loca-commercial','App\Http\Controllers\RegisterController@mitralocacommercial')->name('mitra-loca-commercial');

Route::get('/detail-mitra-loca','App\Http\Controllers\RegisterController@detailmitraloca')->name('detail-mitra-loca');



Route::get('/mitra-jawara','App\Http\Controllers\RegisterController@mitrajawara')->name('mitra-jawara');

Route::get('/detail-mitra-japang','App\Http\Controllers\RegisterController@detailmitrajapang')->name('detail-mitra-japang');

Route::get('/edit-mitra-japang','App\Http\Controllers\RegisterController@editmitrajapang')->name('edit-mitra-japang');

Route::post('/status-jawara','App\Http\Controllers\RegisterController@statusjawara')->name('status-jawara');



Route::get('/mitra-jawara-aktif','App\Http\Controllers\RegisterController@mitrajawaraaktif')->name('mitra-jawara-aktif');

Route::get('/mitra-jawara-non-aktif','App\Http\Controllers\RegisterController@mitrajawaranonaktif')->name('mitra-jawara-non-aktif');

Route::get('/user','App\Http\Controllers\UserController@index')->name('user');
Route::get('/edit-user','App\Http\Controllers\UserController@edit')->name('edit-user');
Route::post('/update-user','App\Http\Controllers\UserController@update')->name('update-user');

Route::get('/non-aktif-user','App\Http\Controllers\UserController@non_aktif')->name('non-aktif-user');
Route::get('/aktif-user','App\Http\Controllers\UserController@aktif')->name('aktif-user');
Route::get('/tambah-user','App\Http\Controllers\UserController@tambah')->name('tambah-user');
Route::post('/insert-user','App\Http\Controllers\UserController@insertuser')->name('insert-user');




Route::get('/company','App\Http\Controllers\CompanyController@index')->name('company');
Route::get('/tambah-company','App\Http\Controllers\CompanyController@tambah')->name('tambah-company');

Route::post('/simpan-company','App\Http\Controllers\CompanyController@simpan')->name('simpan-company');


Route::get('/bisnis','App\Http\Controllers\BisnisController@index')->name('bisnis');
Route::get('/bisnis_export','App\Http\Controllers\BisnisController@export_excel')->name('bisnis_export');


Route::get('/lokasi_bisnis','App\Http\Controllers\LokasiBisnisController@index')->name('lokasi_bisnis');
Route::get('/lokasi_bisnis_export','App\Http\Controllers\LokasiBisnisController@export_excel')->name('lokasi_bisnis_export');

Route::get('/transaksi','App\Http\Controllers\TransaksiController@index')->name('transaksi');

Route::get('/transaksi_isat','App\Http\Controllers\TransaksiController@transaksi_isat')->name('transaksi_isat');

Route::post('/filter-trx','App\Http\Controllers\TransaksiController@filter_trx')->name('filter-trx');








