@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Data user</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <form id="form-input">
                <input type="hidden" name="ip" id="ip">
              <div class="form-group">
                <label for="inputName">Username</label>
                <input type="text" id="username" name="username" class="form-control">
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputDescription">First Name</label>
                <input type="text" id="first_name" name="first_name" class="form-control">
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Last Name</label>
                <input type="text" id="last_name" name="last_name" class="form-control">
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Email</label>
                <input type="text" id="email" name="email" class="form-control" >
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Phone</label>
                <input type="text" id="phone" name="phone" class="form-control" >
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Referall Code</label>
                <input type="text" id="referall_code" name="referall_code" class="form-control">
              </div>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <input type="button" value="Submit" onclick="simpan()" class="btn btn-success float-right">
        </div>
      </div>
      <br>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
   function simpan() {
    if($('#username').val()===''){
      $( "#username" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#username").removeClass( "is-invalid" );

    }

    if($('#first_name').val()===''){
      $( "#first_name" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#first_name").removeClass( "is-invalid" );

    }

    if($('#last_name').val()===''){
      $( "#last_name" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#last_name").removeClass( "is-invalid" );

    }

    if($('#email').val()===''){
      $( "#email" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#email").removeClass( "is-invalid" );

    }
    if($('#phone').val()===''){
      $( "#phone" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#phone").removeClass( "is-invalid" );

    }

    

    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('insert-user') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        console.log('===========');
        console.log(response['statusCode']);
        console.log(response.statusCode);
        console.log('===========');

        if(response.statusCode!=200){
          swal.fire("info",response.message,"info");
        }else{
         swal.fire({
                 title: "Info",
                 text: "Data Berhasil Diubah",
                 type: "success",
                 confirmButtonText: "Tutup",
                 closeOnConfirm: true
              }).then(function(result){
                  if (result.value) {
                      window.location.href = '{{ route('user') }}';
                  }
              });

        }

        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Terjadi Kesalahan',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   

  }
  </script>
  <script type="application/javascript">
  function getIP(json) {
    //var x =document.write("My public IP address is: ", json.ip);
    document.getElementById("ip").value =json.ip;
  }
</script>

<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP"></script>
@stop