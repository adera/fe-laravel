<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>

<table class="tg" border="1">
 <thead>
  <tr>
    <th title="Field #1">No</th>
    <th title="Field #2">Bisnis</th>
    <th title="Field #3">Location</th>
    <th title="Field #4">Nama</th>
    <th title="Field #5">Alamat</th>
    <th title="Field #6">Status</th>
  </tr>
</thead>
<tbody>
  @php
  $no=0;
  @endphp 
    @foreach($data as $item)
    @php
    $no++;
    $id=$item['id'];
    @endphp
      <tr>        
      <td>{{$no}}</td>
      <td>{{$item['bisnis']}}</td>
      <td>{{$item['location_id']}}</td>
      <td>{{$item['name']}}</td>
      <td>{{$item['landmark']}}</td>
      <td>
        @if($item['is_active']==1)
        Aktif
        @else
        Tidak Aktif
        @endif
      </td>
      </tr>
    @endforeach
    </tbody>
</table>