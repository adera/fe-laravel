@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Company</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah Company</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Data Company</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <form id="form-input">
                <input type="hidden" name="ip" id="ip">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputName">Nama</label>
                      <input type="text" id="nama" name="nama" class="form-control">
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputName">Tipe Company</label>
                      <select class="form-control" id="tipe" name="tipe">
                         <option value="">Silankan Pilih</option>
                         @foreach($company as $item)
                         <option value="{{$item['id']}}">{{$item['name']}}</option>
                         @endforeach 
                      </select>
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                  </div> 
                </div> 
              <div class="form-group">
                <label for="inputClientCompany">Alamat</label>
                <textarea class="form-control" id="alamat" name="alamat"></textarea>
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputProjectLeader">Provinsi</label>
                    <select class="form-control" id="provinsi" name="provinsi">
                         <option value="">Silankan Pilih</option>
                         @foreach($provinsi as $item)
                         <option value="{{$item['id']}}">{{$item['name']}}</option>
                         @endforeach  
                    </select>
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputProjectLeader">Kota/Kab</label>
                      <select class="form-control" id="kota" name="kota">
                         <option value="">Silankan Pilih</option> 
                      </select>
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                  </div>
              </div> 
              
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputProjectLeader">Kecamatan</label>
                      <select class="form-control" id="kecamatan" name="kecamatan">
                         <option value="">Silankan Pilih</option> 
                      </select>
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputProjectLeader">Kelurahan</label>
                      <select class="form-control" id="kelurahan" name="kelurahan">
                         <option value="">Silankan Pilih</option> 
                      </select>
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputProjectLeader">No Telp</label>
                      <input type="text" id="phone" name="phone" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputProjectLeader">Kodepos</label>
                      <input type="text" id="kodepos" name="kodepos" class="form-control">
                    </div>
                  </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>


          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Data user</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Username</label>
                <input type="text" id="username" name="username" class="form-control">
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputDescription">First Name</label>
                      <input type="text" id="first_name" name="first_name" class="form-control">
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                   </div>
                   <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputClientCompany">Last Name</label>
                      <input type="text" id="last_name" name="last_name" class="form-control">
                      <div class="invalid-feedback">Silahkan isi</div>
                    </div>
                   </div>
              </div>      
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputProjectLeader">Email</label>
                    <input type="text" id="email" name="email" class="form-control" >
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputProjectLeader">Phone</label>
                    <input type="text" id="phone1" name="phone1" class="form-control" >
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
                </div>
              </div>    
              
              
              
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <input type="button" value="Submit" onclick="simpan()" class="btn btn-success float-right">
        </div>
      </div>
      <br>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
      $('#provinsi').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kota')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kota').html(_items);
          }
      });

  });
  $('#kota').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kecamatan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kecamatan').html(_items);
          }
      });

  });

     $('#kecamatan').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kelurahan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kelurahan').html(_items);
          }
      });

  });
   function simpan() {
    if($('#nama').val()===''){
      $( "#nama" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#nama").removeClass( "is-invalid" );

    } 

    if($('#tipe').val()===''){
      $( "#tipe" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#tipe").removeClass( "is-invalid" );

    } 

    if($('#alamat').val()===''){
      $( "#alamat" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#alamat").removeClass( "is-invalid" );

    }

    if($('#provinsi').val()===''){
      $( "#provinsi" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#provinsi").removeClass( "is-invalid" );

    }  

    if($('#kota').val()===''){
      $( "#kota" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kota").removeClass( "is-invalid" );

    } 
    if($('#kecamatan').val()===''){
      $( "#kecamatan" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kecamatan").removeClass( "is-invalid" );

    } 
    if($('#kelurahan').val()===''){
      $( "#kelurahan" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kelurahan").removeClass( "is-invalid" );

    }

    if($('#phone').val()===''){
      $( "#phone" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#phone").removeClass( "is-invalid" );

    }

    if($('#kodepos').val()===''){
      $( "#kodepos" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kodepos").removeClass( "is-invalid" );

    }   



    if($('#username').val()===''){
      $( "#username" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#username").removeClass( "is-invalid" );

    }

    if($('#first_name').val()===''){
      $( "#first_name" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#first_name").removeClass( "is-invalid" );

    }

    if($('#last_name').val()===''){
      $( "#last_name" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#last_name").removeClass( "is-invalid" );

    }

    if($('#email').val()===''){
      $( "#email" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#email").removeClass( "is-invalid" );

    }
    if($('#phone1').val()===''){
      $( "#phone1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#phone1").removeClass( "is-invalid" );

    }

    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('simpan-company') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        console.log('===========');
        console.log(response['statusCode']);
        console.log(response.statusCode);
        console.log('===========');

        if(response.statusCode!=200){
          swal.fire("info",response.message,"info");
        }else{
         swal.fire({
                 title: "Info",
                 text: "Data Berhasil Disubmit",
                 type: "success",
                 confirmButtonText: "Tutup",
                 closeOnConfirm: true
              }).then(function(result){
                  if (result.value) {
                      window.location.href = '{{ route('company') }}';
                  }
              });

        }

        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Terjadi Kesalahan',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   

  }
  </script>
  <script type="application/javascript">
  function getIP(json) {
    //var x =document.write("My public IP address is: ", json.ip);
    document.getElementById("ip").value =json.ip;
  }
</script>

<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP"></script>
@stop