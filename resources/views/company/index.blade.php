@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Company</h1>
            
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Master Company</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

       <div class="card">
        
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group">
                <label for="inputName"><a href="{{route('tambah-company')}}" class="btn btn-block btn-outline-info btn-sm">Tambah</a> </label>
              </div>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Provinsi</th>
                  <th>Kota/Kab</th>
                  <th>Kecamatan</th>
                  <th>Kelurahan</th>
                  <th>Type</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php
                $no=0;
                @endphp  
                @foreach($data as $item)
                @php
                $no++;
                $id=$item['id'];
                @endphp
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item['name']}}</td>
                  <td>{{$item['phone']}}</td>
                  <td>{{$item['provinsi']}}</td>
                  <td>{{$item['kota']}}</td>
                  <td>{{$item['kecamatan']}}</td>
                  <td>{{$item['kelurahan']}}</td>
                  <td>{{$item['type_company']}}</td>
                  <td>
                    <button type="button" onclick="detail({{$id}})" class="btn btn-block btn-outline-info btn-sm">Edit</button>
                    
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Provinsi</th>
                  <th>Kota/Kab</th>
                  <th>Kecamatan</th>
                  <th>Kelurahan</th>
                  <th>Kodepos</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function detail(id) {
      window.location.href = '{{ route('edit-user') }}?id='+id;
    }
    function non_aktif(id) {
      Swal.fire({
          title: 'Info',
          text: "Non Aktifkan user?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya'
        }).then((result) => {
          if (result.isConfirmed) {
             $.ajax({
              type: 'GET',
              url: '{{ route('non-aktif-user') }}?id='+id,
              contentType: false,
              cache: false,
              processData: false,

              beforeSend: function () {
                $("#loading").css('display', 'block');
              },

              success: function (response) {
                console.log(response);
                var response=JSON.parse(response);
                $("#loading").css('display', 'none');

                console.log('===========');
                console.log(response['statusCode']);
                console.log(response.statusCode);
                console.log('===========');

                if(response.statusCode!=200){
                  swal.fire("info",response.message,"info");
                }else{
                 swal.fire({
                         title: "Info",
                         text: "User Berhasil Dinonaktifkan",
                         type: "success",
                         confirmButtonText: "Tutup",
                         closeOnConfirm: true
                      }).then(function(result){
                          if (result.value) {
                              window.location.href = '{{ route('user') }}';
                          }
                      });

                }

                
              }

            }).done(function (msg) {
              $("#loading").css('display', 'none');
            }).fail(function (response) {
              console.log(objData);
              $("#loading").css('display', 'none');
              swal.fire("error",'Terjadi Kesalahan',"error");
              // toastr.error("Terjadi Kesalahan");
            });

        }
      });
    }




    function aktif(id) {
      Swal.fire({
          title: 'Info',
          text: "Aktifkan user?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya'
        }).then((result) => {
          if (result.isConfirmed) {
             $.ajax({
              type: 'GET',
              url: '{{ route('aktif-user') }}?id='+id,
              contentType: false,
              cache: false,
              processData: false,

              beforeSend: function () {
                $("#loading").css('display', 'block');
              },

              success: function (response) {
                console.log(response);
                var response=JSON.parse(response);
                $("#loading").css('display', 'none');

                console.log('===========');
                console.log(response['statusCode']);
                console.log(response.statusCode);
                console.log('===========');

                if(response.statusCode!=200){
                  swal.fire("info",response.message,"info");
                }else{
                 swal.fire({
                         title: "Info",
                         text: "User Berhasil Diaktifkan",
                         type: "success",
                         confirmButtonText: "Tutup",
                         closeOnConfirm: true
                      }).then(function(result){
                          if (result.value) {
                              window.location.href = '{{ route('user') }}';
                          }
                      });

                }

                
              }

            }).done(function (msg) {
              $("#loading").css('display', 'none');
            }).fail(function (response) {
              console.log(objData);
              $("#loading").css('display', 'none');
              swal.fire("error",'Terjadi Kesalahan',"error");
              // toastr.error("Terjadi Kesalahan");
            });

        }
      });
    }
  </script>
@stop