@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Transaksi Indosat</h1>
            
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Master Transaksi Indosat</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

       <div class="card">
        
            <!-- /.card-header -->
            <div class="card-body">
              <form id="form-input" method="GET">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="inputName">Grup</label>
                      <select class="form-control" name="grup">
                        <option value="Bisnis">Bisnis</option>
                        <option value="Provinsi">Provinsi</option>
                        <option value="Kota">Kota</option>
                      </select>
                    </div>
                  </div>  
                  <div class="col-6">
                    <div class="form-group">
                      <label for="inputName">Tanggal</label>
                      <input type="text" class="form-control" name="reportrange" id="reportrange" readonly>
                    </div>
                  </div>    
                </div>
                 <div class="row">
                  <div class="col-12">
                    <button type="submit" name="Filter" value="Filter" class="btn btn-success float-right">Filter</button>
                  </div>
                </div>
              </form>

             
              
            </div>
            <!-- /.card-body -->
          </div>

       <div class="card">
        
            <!-- /.card-header -->
            <div class="card-body">
              <div id="container11">
              </div>
              @php
              $tss=array();
              $dataa=array();
              $data1=array();
              if(isset($data)){
                foreach($data['data']['brands'] as $item){
                  $tss=array($item['name']);
                }
                $dataa=$data['data']['data'];
                 

                               
              }
              $tss1=json_encode($tss);
              $tss2=json_encode($dataa);
              @endphp
              <input type="hidden" name="tess" id="tess" value="{{$tss1}}">
              <input type="hidden" name="tess1" id="tess1" value="{{$tss2}}">
            </div>
            <!-- /.card-body -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>

<script src="{{asset('public/assets/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/chart/modernizr.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('public/chart/jquery.min.js')}}"></script>
<script src="{{asset('public/chart/highcharts.js')}}"></script>
<script src="{{asset('public/chart/grid-light.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script type="text/javascript">
   $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hari Ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Dalam 7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
           'Dalam 30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Tahun Ini': [moment().startOf('year'), moment().endOf('year')],
           'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

    cb(start, end);
    
});
  /** Bar chart, two columns each **/
    //alert(JSON.parse($('#tess').val()));

    $('#container11').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Grafik Transaksi'
        },
        xAxis: {
            categories: JSON.parse($('#tess').val())
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        series: JSON.parse($('#tess1').val())
    });
</script>

<script type="text/javascript">
   function simpan() {
    
    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('filter-trx') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var dt_lbl=[];
        var dt=[];
        var dt_bg=[];
        var dt_br=[];
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        
        $.each(response['data'], function (k,v) {
            dt_lbl.push(v.name);
            dt.push(v.quantity);
            dt_bg.push('rgba(54, 162, 235, 0.2)');
            dt_br.push('rgba(54, 162, 235, 1)');
        });
        

        console.log('===========');
        console.log(dt_lbl);
        console.log(dt);
        console.log(dt_bg);
        console.log(dt_br);
        console.log('===========');
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Terjadi Kesalahan',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   }

  

</script>
@stop