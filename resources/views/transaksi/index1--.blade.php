@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Transaksi Indosat</h1>
            
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Master Transaksi Indosat</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

       <div class="card">
        
            <!-- /.card-header -->
            <div class="card-body">
              <form id="form-input">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="inputName">Grup</label>
                      <select class="form-control" name="grup">
                        <option value="Bisnis">Bisnis</option>
                        <option value="Provinsi">Provinsi</option>
                        <option value="Kota">Kota</option>
                      </select>
                    </div>
                  </div>  
                  <div class="col-6">
                    <div class="form-group">
                      <label for="inputName">Tanggal</label>
                      <input type="text" class="form-control" name="reportrange" id="reportrange" readonly>
                    </div>
                  </div>    
                </div>
                
              </form>

              <div class="row">
                <div class="col-12">
                  <button type="button" onclick="simpan()" class="btn btn-success float-right">Filter</button>
                </div>
              </div>
              
            </div>
            <!-- /.card-body -->
          </div>

       <div class="card">
        
            <!-- /.card-header -->
            <div class="card-body">
              <div id="container11">
  <canvas id="canvas"></canvas>
</div>
            </div>
            <!-- /.card-body -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script  src="{{asset('public/chart/modernizr.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('public/chart/jquery.min.js')}}"></script>
<script src="{{asset('public/chart/highcharts.js')}}"></script>
<script src="{{asset('public/chart/grid-light.js')}}"></script>
-->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js'></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script type="text/javascript">
  $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hari Ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Dalam 7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
           'Dalam 30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Tahun Ini': [moment().startOf('year'), moment().endOf('year')],
           'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

    cb(start, end);
    
});
   function simpan() {
     var dt_lbl=[];
  var dt=[];
  var dt_bg=[];
  var dt_br=[];
    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('filter-trx') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');
        console.log('===========');
        console.log(response);
        console.log('===========');
        
        $.each(response['data']['brands'], function (k,v) {
            dt_lbl.push(v.brands);
        });
        $.each(response['data']['data'], function (k,v) {
            dt.push(v.qty);
        });
        
        $.each(response['data']['name'], function (k,v) {
            dt_bg.push({
      label: v.name,
      backgroundColor: "pink",
      borderColor: "red",
      borderWidth: 1,
      data: dt
    });
        });
        

        console.log('===========');
        console.log(dt);
        console.log(dt_bg);
        console.log('===========');
        
        
        barChartData = {
  labels:dt_lbl,
  datasets: dt_bg
};

var chartOptions = {
  responsive: true,
  legend: {
    position: "top"
  },
  title: {
    display: true,
    text: "Grafik Transaksi"
  },
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  }
}
  var ctx = document.getElementById("canvas").getContext("2d");
  window.myBar = new Chart(ctx, {
    type: "bar",
    data: barChartData,
    options: chartOptions
  });


        
        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Tidak Ada Transaksi',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   }
   
  

</script>
<!--<script src="{{asset('public/chart/plugins.js')}}"></script>-->
@stop