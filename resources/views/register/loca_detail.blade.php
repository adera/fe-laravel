@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Register Loca</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Register Loca</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Data Register</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">No Referensi <br> {{$data[0]['reference_no']}}</label>
              </div>
              <div class="form-group">
                <label for="inputDescription">NIK <br> {{$data[0]['nik']}}</label>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Nama <br> {{$data[0]['name']}}</label>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">No Telp <br> {{$data[0]['phone']}}</label>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Email <br> {{$data[0]['email']}}</label>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Status Pinjaman <br> {{$data[0]['self_assesment_loan']}}</label>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Kelengkapan Data</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <form method="post" id="form-input">
                <input type="hidden" name="idregis" value="{{$data[0]['id']}}">
              <div class="form-group">
                <label for="inputEstimatedBudget">Nama Bisnis</label>
                <input type="text" id="nama_bisnis" name="nama_bisnis" class="form-control">
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Type Bisnis</label>
                <select class="form-control custom-select" id="bisnis" name="bisnis">
                  <option Value="">Silahkan Pilih</option>
                  @foreach($bisnis as $item)
                  <option value="{{$item['id']}}">{{$item['name']}}</option>
                  @endforeach
                </select>
                <div class="invalid-feedback">Silahkan Pilih</div>
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Type Register</label>
                <select class="form-control custom-select" id="register" name="register">
                  <option Value="">Silahkan Pilih</option>
                  @foreach($register as $item)
                  <option value="{{$item['id']}}">{{$item['name']}}</option>
                  @endforeach
                </select>
                <div class="invalid-feedback">Silahkan Pilih</div>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Alamat</label>
                <textarea id="alamat" name="alamat" class="form-control" rows="4"></textarea>
                <div class="invalid-feedback">Silahkan isi</div>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Provinsi</label>
                <select class="form-control custom-select" id="provinsi" name="provinsi">
                  <option value="">Silahkan Pilih</option>
                  @foreach($provinsi as $item)
                  <option value="{{$item['id']}}">{{$item['name']}}</option>
                  @endforeach
                </select>
                <div class="invalid-feedback">Silahkan Pilih</div>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Kota/Kab</label>
                <select class="form-control custom-select" id="kota" name="kota">
                  <option value="">Silahkan Pilih</option>
                </select>
                <div class="invalid-feedback">Silahkan Pilih</div>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Kecamatan</label>
                <select class="form-control custom-select" id="kecamatan" name="kecamatan">
                  <option value="">Silahkan Pilih</option>
                </select>
                <div class="invalid-feedback">Silahkan Pilih</div>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Kelurahan</label>
                <select class="form-control custom-select" id="kelurahan" name="kelurahan">
                  <option value="">Silahkan Pilih</option>
                </select>
                <div class="invalid-feedback">Silahkan Pilih</div>
              </div>
              
              <div class="form-group">
                <label for="inputEstimatedDuration">Kodepos</label>
                <input type="text" class="form-control" id="kodepos" name="kodepos">
              </div>
              </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <input type="button" value="Submit" onclick="simpan()" class="btn btn-success float-right">
        </div>
      </div>
      <br>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
      $('#provinsi').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kota')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kota').html(_items);
          }
      });

  });
  $('#kota').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kecamatan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kecamatan').html(_items);
          }
      });

  });

     $('#kecamatan').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kelurahan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kelurahan').html(_items);
          }
      });

  });

      function simpan() {
    if($('#nama_bisnis').val()===''){
      $( "#nama_bisnis" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#nama_bisnis").removeClass( "is-invalid" );

    }

    if($('#bisnis').val()===''){
      $( "#bisnis" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#bisnis").removeClass( "is-invalid" );

    }

    if($('#alamat').val()===''){
      $( "#alamat" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#alamat").removeClass( "is-invalid" );

    }

    if($('#provinsi').val()===''){
      $( "#provinsi" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#provinsi").removeClass( "is-invalid" );

    }
    if($('#kota').val()===''){
      $( "#kota" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kota").removeClass( "is-invalid" );

    }
    if($('#kecamatan').val()===''){
      $( "#kecamatan" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kecamatan").removeClass( "is-invalid" );

    }
    if($('#kelurahan').val()===''){
      $( "#kelurahan" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kelurahan").removeClass( "is-invalid" );

    }

    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('simpan-loca') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        console.log('===========');
        console.log(response['statusCode']);
        console.log(response.statusCode);
        console.log('===========');

        if(response.statusCode!=200){
          swal.fire("info",response.message,"info");
        }else{
         swal.fire({
                 title: "Info",
                 text: "Data Berhasil Disubmit",
                 type: "success",
                 confirmButtonText: "Tutup",
                 closeOnConfirm: true
              }).then(function(result){
                  if (result.value) {
                      window.location.href = '{{ route('register-loca') }}';
                  }
              });

        }

        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Terjadi Kesalahan',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   

  }
  </script>
@stop