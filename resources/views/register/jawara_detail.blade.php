@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Register Jawara</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">register Jawara</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Register</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>No Referensi <br> {{$data[0]['reference_no']}}</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                  <label>No Telp <br> {{$data[0]['phone']}}</label>
                </div>

                
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>NIK <br> {{$data[0]['nik']}}</label>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Email <br> {{$data[0]['email']}}</label>
                </div>
                <!-- /.form-group -->
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Nama<br> {{$data[0]['name']}}</label>
                </div>
                <!-- /.form-group -->
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            
          </div>
        </div>
        <!-- /.card -->
<form method="post" id="form-input" enctype="multipart/form-data">
                <input type="hidden" name="idregis" value="{{$data[0]['id']}}">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Alamat Rumah</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Provinsi</label>
                  <select id="provinsi" name="provinsi" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                    @foreach($provinsi as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                    @endforeach
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Kecamatan</label>
                  <select id="kecamatan" name="kecamatan" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kota/Kab</label>
                  <select id="kota" name="kota" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Kelurahan</label>
                  <select id="kelurahan" name="kelurahan" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea id="alamat" name="alamat" class="form-control" rows="4"></textarea>
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label>RT</label>
                    <input type="text" id="rt" name="rt" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                    <label>RW</label>
                    <input type="text" id="rw" name="rw" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                    <label>Kodepos</label>
                    <input type="text" id="kodepos" name="kodepos" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
            </div>

          </div>
          
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Alamat Usaha</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Usaha</label>
                    <input type="text" id="nama_usaha" name="nama_usaha" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea id="alamat1" name="alamat1" class="form-control" rows="4"></textarea>
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Provinsi</label>
                  <select id="provinsi1" name="provinsi1" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                    @foreach($provinsi as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                    @endforeach
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Kecamatan</label>
                  <select id="kecamatan1" name="kecamatan1" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kota/Kab</label>
                  <select id="kota1" name="kota1" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Kelurahan</label>
                  <select id="kelurahan1" name="kelurahan1" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                  </select>
                  <div class="invalid-feedback">Silahkan Pilih</div>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>


        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Dokumen Pendukung</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Upload KTP</label>
                  <input type="file" class="form-control" id="ktp" name="ktp">
                  <div class="invalid-feedback">Silahkan isi</div>
                  <div class="invalid-feedback" id="m_ktp">Size Dokumen Harus Dibawah 300Kb</div>
                  <div class="invalid-feedback" id="m_ktp1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                  <label>Upload KK</label>
                  <input type="file" class="form-control" id="kk" name="kk">
                   <div class="invalid-feedback">Silahkan isi</div>
                   <div class="invalid-feedback" id="m_kk">Size Dokumen Harus Dibawah 300Kb</div>
                      <div class="invalid-feedback" id="m_kk1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Upload NPWP</label>
                  <input type="file" class="form-control" id="npwp" name="npwp">
                   
                   <div class="invalid-feedback" id="m_npwp">Size Dokumen Harus Dibawah 300Kb</div>
                      <div class="invalid-feedback" id="m_npwp1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Upload Foto Lokasi Usaha</label>
                  <input type="file" class="form-control" id="lok_usaha" name="lok_usaha">
                   <div class="invalid-feedback">Silahkan isi</div>
                   <div class="invalid-feedback" id="m_lok_usaha">Size Dokumen Harus Dibawah 300Kb</div>
                      <div class="invalid-feedback" id="m_lok_usaha1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Upload Foto Lainnya</label>
                  <input type="file" class="form-control" id="lain" name="lain">
                  <div class="invalid-feedback" id="m_lain">Size Dokumen Harus Dibawah 300Kb</div>
                      <div class="invalid-feedback" id="m_lain1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Upload Foto Selfie</label>
                  <input type="file" class="form-control" id="selfie" name="selfie">
                   <div class="invalid-feedback" id="m_selfie">Size Dokumen Harus Dibawah 300Kb</div>
                      <div class="invalid-feedback" id="m_selfie1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Upload Foto Selfie Dengan KTP</label>
                  <input type="file" class="form-control" id="selfiektp" name="selfiektp">
                   <div class="invalid-feedback" id="m_selfiektp">Size Dokumen Harus Dibawah 300Kb</div>
                      <div class="invalid-feedback" id="m_selfiektp1">Format File Harus PNG,JPG dan JPEG</div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>


          <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Lainnya</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label>Nama Bank</label>
                    <select id="bank" name="bank" class="form-control select2bs4" style="width: 100%;">
                      <option value="">Silahkan Pilih</option>
                      @foreach($bisnis as $item)
                      <option value="{{$item['id']}}">{{$item['name']}}</option>
                      @endforeach
                    </select>
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
               <div class="col-md-4">
                <div class="form-group">
                    <label>No Rekening</label>
                    <input type="text" id="norek" name="norek" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                    <label>Nama Pemilik</label>
                    <input type="text" id="nama_pemilik" name="nama_pemilik" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div>  
            </div>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label>Apakah Anda Memiliki Pinjaman</label>
                    <select id="pinjaman" name="pinjaman" class="form-control select2bs4" style="width: 100%;">
                    <option value="">Silahkan Pilih</option>
                    <option value="Ya : sudah selesai dan lancar">Ya : sudah selesai dan lancar</option>
                    <option value="Ya : sudah selesai dan mengalami kredit macet">Ya : sudah selesai dan mengalami kredit macet</option>
                    <option value="Ya : sedang berjalan dan mengalami kredit macet">Ya : sedang berjalan dan mengalami kredit macet</option>
                    <option value="Ya : sedang berjalan dan tidak pernah macet">Ya : sedang berjalan dan tidak pernah macet</option>
                    <option value="Tidak Pernah">Tidak Pernah</option>
                  </select>
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="icheck-primary d-inline">
                        <input type="checkbox" id="checkboxPrimary1">
                        <label for="checkboxPrimary1">
                          Proses ini tidak dikenakan biaya tambahan apapun.
                        </label>
                    </div>
                  <div class="invalid-feedback">Silahkan Checklist</div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="icheck-primary d-inline">
                        <input type="checkbox" id="checkboxPrimary2">
                        <label for="checkboxPrimary2">
                          Bagi para jawara yang sudah lolos seleksi dan pantas untuk mendapatkan pendanaan, jika tidak melanjutkan proses (tidak dapat dihubungi, pengunduran diri, dan lainnya) akan berdampak pada sistem blacklist untuk seluruh anggota keluarga dalam kartu keluarga sehingga tidak dapat mengajukan di kedepan harinya.
                        </label>
                    </div>
                  <div class="invalid-feedback">Silahkan Checklist</div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div class="icheck-primary d-inline">
                        <input type="checkbox" id="checkboxPrimary3">
                        <label for="checkboxPrimary3">
                          Pengunduran diri setelah penandatanganan akan dikenakan biaya penalty 5% dari total nilai pengajuan dana.
                        </label>
                    </div>
                  <div class="invalid-feedback">Silahkan Checklist</div>
                </div>
              </div>
            </div>


            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>
        </form>
        </div>
        <div class="row">
        <div class="col-12">
          <input type="button" value="Submit" onclick="simpan()" class="btn btn-success float-right">
        </div>
      </div>
        <br>








      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
      $('#provinsi').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kota')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kota').html(_items);
          }
      });

  });
  $('#kota').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kecamatan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kecamatan').html(_items);
          }
      });

  });

     $('#kecamatan').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kelurahan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kelurahan').html(_items);
          }
      });

  });


     $('#provinsi1').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kota')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kota1').html(_items);
          }
      });

  });
  $('#kota1').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kecamatan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kecamatan1').html(_items);
          }
      });

  });

     $('#kecamatan1').on('change', function (v) {

    var _items='';
    $.ajax({
          type: 'GET',
          url: '{{route('kelurahan')}}?id='+this.value,
          success: function (res) {
              var data = $.parseJSON(res);
              console.log(data);
              _items='<option value="">Silahkan Pilih</option>';
              $.each(data, function (k,v) {
                  _items += "<option value='"+v.id+"'>"+v.name+"</option>";
              });

              $('#kelurahan1').html(_items);
          }
      });

  });

  function simpan() {
    if($('#provinsi').val()===''){
      $( "#provinsi" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#provinsi").removeClass( "is-invalid" );

    }
    if($('#kota').val()===''){
      $( "#kota" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kota").removeClass( "is-invalid" );

    }
    if($('#kecamatan').val()===''){
      $( "#kecamatan" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kecamatan").removeClass( "is-invalid" );

    }
    if($('#kelurahan').val()===''){
      $( "#kelurahan" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kelurahan").removeClass( "is-invalid" );

    }

    if($('#alamat').val()===''){
      $( "#alamat" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#alamat").removeClass( "is-invalid" );

    }

    if($('#rt').val()===''){
      $( "#rt" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#rt").removeClass( "is-invalid" );

    }
    if($('#rw').val()===''){
      $( "#rw" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#rw").removeClass( "is-invalid" );

    }

    if($('#kodepos').val()===''){
      $( "#kodepos" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kodepos").removeClass( "is-invalid" );

    }

    if($('#nama_usaha').val()===''){
      $( "#nama_usaha" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#nama_usaha").removeClass( "is-invalid" );

    }

    if($('#alamat1').val()===''){
      $( "#alamat1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#alamat1").removeClass( "is-invalid" );

    }

    if($('#provinsi1').val()===''){
      $( "#provinsi1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#provinsi1").removeClass( "is-invalid" );

    }
    if($('#kota1').val()===''){
      $( "#kota1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kota1").removeClass( "is-invalid" );

    }
    if($('#kecamatan1').val()===''){
      $( "#kecamatan1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kecamatan1").removeClass( "is-invalid" );

    }
    if($('#kelurahan1').val()===''){
      $( "#kelurahan1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#kelurahan1").removeClass( "is-invalid" );

    }


    if($('#ktp').val()===''){
      $( "#ktp" ).addClass( "is-invalid" );
      $("#m_ktp").hide();
      $("#m_ktp1").hide();
      return false;
    }else{
      const fsize = document.getElementById('ktp').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_ktp").show();
        $("#m_ktp1").hide();
      } else {

        fileName = document.querySelector('#ktp').value;
        extension = fileName.split('.').pop();
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_ktp1").hide();
        }else{
          $("#m_ktp1").show();
        }
        $("#m_ktp").hide();
      }

      
      $("#ktp").removeClass( "is-invalid" );
    }

    if($('#kk').val()===''){
      $( "#kk" ).addClass( "is-invalid" );
      $("#m_kk").hide();
      $("#m_kk1").hide();
      return false;
    }else{
      const fsize = document.getElementById('kk').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_kk").show();
        $("#m_kk1").hide();
      } else {

        fileName = document.querySelector('#kk').value;
        extension = fileName.split('.').pop();
        
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_kk1").hide();
        }else{
          $("#m_kk1").show();
        }
        $("#m_kk").hide();
      }

      
      $("#kk").removeClass( "is-invalid" );
    }

    if($('#npwp').val()!=''){
     
      const fsize = document.getElementById('npwp').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_npwp").show();
        $("#m_npwp1").hide();
      } else {

        fileName = document.querySelector('#npwp').value;
        extension = fileName.split('.').pop();
        
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_npwp1").hide();
        }else{
          $("#m_npwp1").show();
        }
        $("#m_npwp").hide();
      }

      
      $("#npwp").removeClass( "is-invalid" );
    }


    if($('#lain').val()!=''){
      const fsize = document.getElementById('lain').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_lain").show();
        $("#m_lain1").hide();
      } else {

        fileName = document.querySelector('#lain').value;
        extension = fileName.split('.').pop();
        
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_lain1").hide();
        }else{
          $("#m_lain1").show();
        }
        $("#m_lain").hide();
      }

      
      $("#lain").removeClass( "is-invalid" );
    }




    if($('#selfie').val()!=''){
     
      const fsize = document.getElementById('selfie').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_selfie").show();
        $("#m_selfie1").hide();
      } else {

        fileName = document.querySelector('#selfie').value;
        extension = fileName.split('.').pop();
        
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_selfie1").hide();
        }else{
          $("#m_selfie1").show();
        }
        $("#m_selfie").hide();
      }

      
      $("#selfie").removeClass( "is-invalid" );
    }

    if($('#selfiektp').val()!=''){
      
      const fsize = document.getElementById('selfiektp').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_selfiektp").show();
        $("#m_selfiektp1").hide();
      } else {

        fileName = document.querySelector('#selfiektp').value;
        extension = fileName.split('.').pop();
        
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_selfiektp1").hide();
        }else{
          $("#m_selfiektp1").show();
        }
        $("#m_selfiektp").hide();
      }

      
      $("#selfiektp").removeClass( "is-invalid" );
    }


    if($('#lok_usaha').val()===''){
      $( "#lok_usaha" ).addClass( "is-invalid" );
      $("#m_lok_usaha").hide();
      $("#m_lok_usaha1").hide();
      return false;
    }else{
      const fsize = document.getElementById('lok_usaha').files[0].size;
      const file = Math.round((fsize / 1024));
      if (file >= 309) {
        $("#m_lok_usaha").show();
        $("#m_lok_usaha1").hide();
      } else {

        fileName = document.querySelector('#lok_usaha').value;
        extension = fileName.split('.').pop();
        
        if(extension == 'png' || extension == 'PNG' || extension == 'jpg' || extension == 'JPG' || extension == 'jpeg' || extension == 'JPEG'){
          $("#m_lok_usaha1").hide();
        }else{
          $("#m_lok_usaha1").show();
        }
        $("#m_lok_usaha").hide();
      }

      
      $("#lok_usaha").removeClass( "is-invalid" );
    }

    if($('#bank').val()===''){
      $( "#bank" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#bank").removeClass( "is-invalid" );

    }

    if($('#norek').val()===''){
      $( "#norek" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#norek").removeClass( "is-invalid" );

    }

    if($('#nama_pemilik').val()===''){
      $( "#nama_pemilik" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#nama_pemilik").removeClass( "is-invalid" );

    }


     var checkBox = document.getElementById("checkboxPrimary1");
     var checkBox1 = document.getElementById("checkboxPrimary2");
     var checkBox2 = document.getElementById("checkboxPrimary3");

     if(checkBox.checked == false){
      $( "#checkboxPrimary1" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#checkboxPrimary1").removeClass( "is-invalid" );

    }

    if(checkBox1.checked == false){
      $( "#checkboxPrimary2" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#checkboxPrimary2").removeClass( "is-invalid" );

    }

    if(checkBox2.checked == false){
      $( "#checkboxPrimary3" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#checkboxPrimary3").removeClass( "is-invalid" );

    }


    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('simpan-jawara') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        console.log('===========');
        console.log(response['statusCode']);
        console.log(response.statusCode);
        console.log('===========');

        if(response.statusCode!=200){
          swal.fire("info",response.message,"info");
        }else{
         swal.fire({
                 title: "Info",
                 text: "Data Berhasil Disubmit",
                 type: "success",
                 background: 'white',
                 confirmButtonText: "Tutup",
                 closeOnConfirm: true
              }).then(function(result){
                  if (result.value) {
                      window.location.href = '{{ route('register-jawara') }}';
                  }
              });

        }

        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Terjadi Kesalahan',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   

  }
  </script>
@stop