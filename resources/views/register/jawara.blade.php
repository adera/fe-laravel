@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Register Jawara</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Register Jawara</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Referensi</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php
                $no=0;
                @endphp  
                @foreach($data as $item)
                @php
                $no++;
                @endphp
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item['reference_no']}}</td>
                  <td>{{$item['nik']}}</td>
                  <td>{{$item['name']}}</td>
                  <td>{{$item['phone']}}</td>
                  <td>{{$item['email']}}</td>
                  <td>

                    <a href="http://wa.me/{{$item['phone']}}?text=Selamat Pendaftaran Anda Untuk Menjadi Calon Jawara (Japang Warung Rakyat Sudah Diterima) No Registrasi :{{$item['reference_no']}} Nama :{{$item['name']}}%0AUntuk Proses Selanjutnya Silahkan Melengkapi Persyaratan Berikut :
%0A1. Foto KTP
%0A2. Alamat Lengkap :
%0A3. RT/RW :
%0A4. Kelurahan / Desa :
%0A5. Kecamatan :
%0A6. Kode Pos :
%0A7. Kota/ Kabupaten :
%0A8. Provinsi :
%0A9. Foto KK
%0A10. Foto NPWP (Jika Ada)
%0A11. Foto Selfie
%0A12. Foto Selfie Dengan KTP
%0A13. Jawab Pertanyaan Dibawah ini :
%0A  Apakah Anda Memiliki Pinjaman Di Bank / Finansial Institusi Lain?
%0A  a. Ya : sudah selesai dan lancar
%0A  b. Ya : sudah selesai dan mengalami kredit macet
%0A  c. Ya : sedang berjalan dan mengalami kredit macet
%0A  d. Ya : sedang berjalan dan tidak pernah macet
%0A  e. Tidak Pernah
%0A14. Foto Lokasi Usaha
%0A15. Alamat Usaha ( Alamat Lengkap , RT/RW, Kelurahan /Desa, Kecamatan,Kota/Kabupaten,Provinsi)

%0A 
%0Aini adalah pesan otomatis , silahkan isi data yang diperlukan
%0Aterima kasih
%0ASalam Hangat
%0A
%0ALoca Indonesia
%0APartner Akuisisi 
%0APT Jaring Pangan Indonesia" target="_blank" class="btn btn-block btn-outline-success btn-sm">Copy Url</a>

                    <button type="button" onclick="detail({{$item['id']}})" class="btn btn-block btn-outline-info btn-sm">Verifikasi</button>
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Referensi</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
              <input type="hidden" id="myInput">
            </div>
            <!-- /.card-body -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
   <script type="text/javascript">
    function detail(id) {
      window.location.href = '{{ route('detail-jawara') }}?id='+id;
    }
    function myFunction(id) {
      /* Get the text field */
      var copyText = document.getElementById("myInput");

      /* Select the text field */
      //copyText.select();
      //copyText.setSelectionRange(0, 99999); /* For mobile devices */
      var copyText1 ='';
      $.ajax({
          type: 'GET',
          url: '{{ route('detail_jawara') }}?id='+id,
          success: function (res) {
              var data = $.parseJSON(res);
              
              $.each(data, function (k,v) {
                console.log(v.reference_no);
                    copyText1='http://wa.me/6281222048553?text=Selamat Pendaftaran Anda Untuk Menjadi Calon Jawara (Japang Warung Rakyat Sudah Diterima) No Registrasi :'+v.reference_no+' Nama :'+v.name+'%0AUntuk Proses Selanjutnya Silahkan Melengkapi Persyaratan Berikut :'+
'%0A1. Foto KTP'+
'%0A2. Alamat Lengkap :'+
'%0A3. RT/RW :'+
'%0A4. Kelurahan / Desa :'+
'%0A5. Kecamatan :'+
'%0A6. Kode Pos :'+
'%0A7. Kota/ Kabupaten :'+
'%0A8. Provinsi :'+
'%0A9. Foto KK'+
'%0A10. Foto NPWP (Jika Ada)'+
'%0A11. Foto Selfie'+
'%0A12. Foto Selfie Dengan KTP'+
'%0A13. Jawab Pertanyaan Dibawah ini :'+
'%0A  Apakah Anda Memiliki Pinjaman Di Bank / Finansial Institusi Lain?'+
'%0A  a. Ya : sudah selesai dan lancar'+
'%0A  b. Ya : sudah selesai dan mengalami kredit macet'+
'%0A  c. Ya : sedang berjalan dan mengalami kredit macet'+
'%0A  d. Ya : sedang berjalan dan tidak pernah macet'+
'%0A  e. Tidak Pernah'+
'%0A14. Foto Lokasi Usaha'+
'%0A15. Alamat Usaha ( Alamat Lengkap , RT/RW, Kelurahan /Desa, Kecamatan,Kota/Kabupaten,Provinsi)'+

'%0A '+
'%0Aini adalah pesan otomatis , silahkan isi data yang diperlukan'+
'%0Aterima kasih'+
'%0ASalam Hangat'+
'%0A'+
'%0ALoca Indonesia'+
'%0APartner Akuisisi'+ 
'%0APT Jaring Pangan Indonesia'
              });
               // copyText.select();
           // copyText.setSelectionRange(0, 99999);

            /* Copy the text inside the text field */
           navigator.clipboard.writeText(copyText1);


            /* Alert the copied text */
            alert("Copied the text: " + copyText1);

          }

          

      });
   
    }
  </script>
@stop