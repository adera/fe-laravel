@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Bisnis</h1>
            
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Master Bisnis</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

       <div class="card">
        
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group">
                <label for="inputName"><a href="{{route('bisnis_export')}}" class="btn btn-block btn-outline-info btn-sm">Export Excel</a> </label>
              </div>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Pricing</th>
                  <th>Pricing Type</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @php
                $no=0;
                @endphp  
                @foreach($data as $item)
                @php
                $no++;
                $id=$item['id'];
                @endphp
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item['name']}}</td>
                  <td>{{number_format($item['pricing'])}}</td>
                  <td>{{$item['pricing_type']}}</td>
                  <td>
                    @if($item['is_active']==1)
                    Aktif
                    @else
                    Tidak Aktif
                    @endif
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th> 
                  <th>Nama</th>
                  <th>Pricing</th>
                  <th>Pricing Type</th>
                  <th>Status</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    function detail(id) {
      window.location.href = '{{ route('edit-user') }}?id='+id;
    }
    function non_aktif(id) {
      Swal.fire({
          title: 'Info',
          text: "Non Aktifkan user?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya'
        }).then((result) => {
          if (result.isConfirmed) {
             $.ajax({
              type: 'GET',
              url: '{{ route('non-aktif-user') }}?id='+id,
              contentType: false,
              cache: false,
              processData: false,

              beforeSend: function () {
                $("#loading").css('display', 'block');
              },

              success: function (response) {
                console.log(response);
                var response=JSON.parse(response);
                $("#loading").css('display', 'none');

                console.log('===========');
                console.log(response['statusCode']);
                console.log(response.statusCode);
                console.log('===========');

                if(response.statusCode!=200){
                  swal.fire("info",response.message,"info");
                }else{
                 swal.fire({
                         title: "Info",
                         text: "User Berhasil Dinonaktifkan",
                         type: "success",
                         confirmButtonText: "Tutup",
                         closeOnConfirm: true
                      }).then(function(result){
                          if (result.value) {
                              window.location.href = '{{ route('user') }}';
                          }
                      });

                }

                
              }

            }).done(function (msg) {
              $("#loading").css('display', 'none');
            }).fail(function (response) {
              console.log(objData);
              $("#loading").css('display', 'none');
              swal.fire("error",'Terjadi Kesalahan',"error");
              // toastr.error("Terjadi Kesalahan");
            });

        }
      });
    }




    function aktif(id) {
      Swal.fire({
          title: 'Info',
          text: "Aktifkan user?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya'
        }).then((result) => {
          if (result.isConfirmed) {
             $.ajax({
              type: 'GET',
              url: '{{ route('aktif-user') }}?id='+id,
              contentType: false,
              cache: false,
              processData: false,

              beforeSend: function () {
                $("#loading").css('display', 'block');
              },

              success: function (response) {
                console.log(response);
                var response=JSON.parse(response);
                $("#loading").css('display', 'none');

                console.log('===========');
                console.log(response['statusCode']);
                console.log(response.statusCode);
                console.log('===========');

                if(response.statusCode!=200){
                  swal.fire("info",response.message,"info");
                }else{
                 swal.fire({
                         title: "Info",
                         text: "User Berhasil Diaktifkan",
                         type: "success",
                         confirmButtonText: "Tutup",
                         closeOnConfirm: true
                      }).then(function(result){
                          if (result.value) {
                              window.location.href = '{{ route('user') }}';
                          }
                      });

                }

                
              }

            }).done(function (msg) {
              $("#loading").css('display', 'none');
            }).fail(function (response) {
              console.log(objData);
              $("#loading").css('display', 'none');
              swal.fire("error",'Terjadi Kesalahan',"error");
              // toastr.error("Terjadi Kesalahan");
            });

        }
      });
    }
  </script>
@stop