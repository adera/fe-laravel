<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>LOCA | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
   <!-- Ionicons -->
  
  <!-- icheck bootstrap -->
  
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="{{asset('public/assets/css/mycss.css')}}" rel="stylesheet">
</head>
<body class="hold-transition login-page">
  <div id="loading">
            <div class="lds-facebook">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
<div class="login-box">
  <div class="login-logo">
    <img src="{{asset('public/assets/img/logo4.png')}}" width="50%">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Silahkan Login</p>

      <form method="post" id="form-input">
        <div class="input-group mb-3">
          <input type="text" name="username" id="username" class="form-control" placeholder="Username">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <div class="invalid-feedback">Silahkan isi Username</div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <div class="invalid-feedback">Silahkan isi Password</div>
        </div>
        
      </form>
      <div class="social-auth-links text-center mb-3">
        <a href="#" onclick="simpan()" class="btn btn-block btn-primary">
          <i class="fab fa-arrow-right-to-bracket mr-2"></i> Login
        </a>
      </div>

      {{--
      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div>
      --}}
      <!-- /.social-auth-links -->
      {{--
      <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p>
      --}}
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('public/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('public/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('public/dist/js/adminlte.min.js')}}"></script>
 <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>

<script type="text/javascript">
   $('#form-input').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    simpan();
                }
            });
  function simpan() {
    if($('#username').val()===''){
      $( "#username" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#username").removeClass( "is-invalid" );

    }

    if($('#password').val()===''){
      $( "#password" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#password").removeClass( "is-invalid" );

    }

    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('login1') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        console.log('===========');
        console.log(response['statusCode']);
        console.log(response.statusCode);
        console.log('===========');

        if(response.statusCode!=200){
          swal.fire("info",response.message,"info");
        }else{
         window.location.href = '{{ route('home') }}';

        }

        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      $("#loading").css('display', 'none');
      //swal.fire("error",'Terjadi Kesalahan 1111',"error");
      // toastr.error("Terjadi Kesalahan");
    });

  }
</script>
</body>
</html>
