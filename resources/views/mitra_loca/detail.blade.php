@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Detail Data</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('public/assets/img/faticon1.png')}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$data[0]['name']}}</h3>

                <p class="text-muted text-center">{{$data[0]['nik']}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>No Referensi</b> <a class="float-right">{{$data[0]['reference_no']}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>No Telp</b> <a class="float-right">{{$data[0]['phone']}}</a>
                  </li>
                  <li class="list-group-item">
                    <a>{{$data[0]['email']}}</a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Lokasi Usaha</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Nama Usaha</strong>

                <p class="text-muted">
                  {{$data[0]['business_name']}}
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Lokasi</strong>

                <p class="text-muted">
                  {{$data[0]['address']}} ,
                  {{$data[0]['provinsi']}} ,
                  {{$data[0]['kota']}} ,
                  {{$data[0]['kecamatan']}} ,
                  {{$data[0]['kelurahan']}} ,

                  {{$data[0]['postical_code']}}

                </p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Type Bisnis</strong>

                <p class="text-muted">
                   {{$data[0]['type_bisnis']}}
                </p>

              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@stop