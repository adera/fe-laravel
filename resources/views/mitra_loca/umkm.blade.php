@section('content')
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Mitra Loca UMKM</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Mitra Loca UMKM</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Referensi</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php
                $no=0;
                @endphp  
                @foreach($data as $item)
                @php
                $no++;
                $id=$item['id'];
                @endphp
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item['reference_no']}}</td>
                  <td>{{$item['nik']}}</td>
                  <td>{{$item['name']}}</td>
                  <td>{{$item['phone']}}</td>
                  <td>{{$item['email']}}</td>
                  <td>
                    <button type="button" onclick="detail({{$id}})" class="btn btn-block btn-outline-info btn-sm">Detail</button>
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>No Referensi</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    function detail(id) {
      window.location.href = '{{ route('detail-mitra-loca') }}?id='+id;
    }
  </script>
@stop