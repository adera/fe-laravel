@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Jawara</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Detail Data Jawara</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('public/assets/img/faticon1.png')}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$data[0]['name']}}</h3>

                <p class="text-muted text-center">{{$data[0]['nik']}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>No Referensi</b> <a class="float-right">{{$data[0]['reference_no']}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>No Telp</b> <a class="float-right">{{$data[0]['phone']}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$data[0]['email']}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Status Progress</b> <a class="float-right">{{$data[0]['status']}}</a>
                  </li>

                </ul>
                <a href="#" onclick="simpan()" class="btn btn-primary btn-block"><b>Submit</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Data Pribadi</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Data Usaha</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Dokumen Pendukung</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                 <form class="form-horizontal" id="form-input" method="post">
                  <input type="hidden" name="idregis" value="{{$data[0]['register_id']}}">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                   
                    

                      <strong><i class="fas fa-map-marker-alt mr-1"></i> Lokasi</strong>
                       <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Provinsi</label>
                            <select id="provinsi" name="provinsi" class="form-control select2bs4" style="width: 100%;">
                              <option value="">Silahkan Pilih</option>
                              @foreach($provinsi as $item)
                              <option value="{{$item['id']}}">{{$item['name']}}</option>
                              @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                          </div>
                          <!-- /.form-group -->
                          <div class="form-group">
                            <label>Kecamatan</label>
                            <select id="kecamatan" name="kecamatan" class="form-control select2bs4" style="width: 100%;">
                              <option value="">Silahkan Pilih</option>
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                          </div>
                          <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Kota/Kab</label>
                            <select id="kota" name="kota" class="form-control select2bs4" style="width: 100%;">
                              <option value="">Silahkan Pilih</option>
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                          </div>
                          <!-- /.form-group -->
                          <div class="form-group">
                            <label>Kelurahan</label>
                            <select id="kelurahan" name="kelurahan" class="form-control select2bs4" style="width: 100%;">
                              <option value="">Silahkan Pilih</option>
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                          </div>
                          <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea id="alamat" name="alamat" class="form-control" rows="4"></textarea>
                            <div class="invalid-feedback">Silahkan isi</div>
                          </div>
                       </div> 
                    </div>
                     <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label>RT</label>
                    <input type="text" id="rt" name="rt" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                    <label>RW</label>
                    <input type="text" id="rw" name="rw" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                    <label>Kodepos</label>
                    <input type="text" id="kodepos" name="kodepos" class="form-control">
                    <div class="invalid-feedback">Silahkan isi</div>
                  </div>
               </div> 
            </div>
                      {{--
                      <p class="text-muted">
                        {{$data[0]['address']}} &nbsp; Rt &nbsp;{{$data[0]['rt']}}/Rw &nbsp;{{$data[0]['rw']}},
                        Provinsi &nbsp; {{$data[0]['provinsi']}} ,
                        Kota / Kab &nbsp;{{$data[0]['kota']}} ,
                        Kecamatan &nbsp; {{$data[0]['kecamatan']}} ,
                        Kelurahan &nbsp; {{$data[0]['kelurahan']}} ,
                        Kodepos &nbsp; {{$data[0]['postical_code']}}

                      </p>
                      --}}
                      <hr>

                      <strong><i class="fas fa-pencil-alt mr-1"></i> Status Pinjaman Lain</strong>
                      <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <select id="pinjaman" name="pinjaman" class="form-control select2bs4" style="width: 100%;">
                            <option value="">Silahkan Pilih</option>
                            <option value="Ya : sudah selesai dan lancar">Ya : sudah selesai dan lancar</option>
                            <option value="Ya : sudah selesai dan mengalami kredit macet">Ya : sudah selesai dan mengalami kredit macet</option>
                            <option value="Ya : sedang berjalan dan mengalami kredit macet">Ya : sedang berjalan dan mengalami kredit macet</option>
                            <option value="Ya : sedang berjalan dan tidak pernah macet">Ya : sedang berjalan dan tidak pernah macet</option>
                            <option value="Tidak Pernah">Tidak Pernah</option>
                          </select>
                            <div class="invalid-feedback">Silahkan isi</div>
                          </div>
                       </div> 
                    </div>
                      {{--
                      <p class="text-muted">
                        {{$data[0]['self_assesment_loan']}}
                      </p>
                      --}}
                      <hr>

                      <strong><i class="fas fa-book mr-1"></i> Bank</strong>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                              <label>Nama Bank</label>
                              <select id="bank" name="bank" class="form-control select2bs4" style="width: 100%;">
                                <option value="">Silahkan Pilih</option>
                                @foreach($bisnis as $item)
                                <option value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                              </select>
                              <div class="invalid-feedback">Silahkan isi</div>
                            </div>
                         </div> 
                         <div class="col-md-4">
                          <div class="form-group">
                              <label>No Rekening</label>
                              <input type="text" id="norek" name="norek" class="form-control">
                              <div class="invalid-feedback">Silahkan isi</div>
                            </div>
                         </div>
                         <div class="col-md-4">
                          <div class="form-group">
                              <label>Nama Pemilik</label>
                              <input type="text" id="nama_pemilik" name="nama_pemilik" class="form-control">
                              <div class="invalid-feedback">Silahkan isi</div>
                            </div>
                         </div>  
                      </div>
                      {{--
                      <p class="text-muted">
                        Nama Bank &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; {{$data[0]['nama_bank']}}
                        <br>
                        No Rekening &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; {{$data[0]['bank_account_number']}}
                        <br>
                        Nama Pemilik &nbsp;:&nbsp; {{$data[0]['bank_account_name']}}
                      </p>
                      --}}  

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <div class="card-body">
                        <strong><i class="fas fa-book mr-1"></i> Nama Usaha</strong>
                        <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                              <label>Nama Usaha</label>
                              <input type="text" id="nama_usaha" name="nama_usaha" class="form-control">
                              <div class="invalid-feedback">Silahkan isi</div>
                            </div>
                         </div> 
                      </div>
                      
                        <p class="text-muted">
                         {{$data[0]['store_name']}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Lokasi</strong>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea id="alamat1" name="alamat1" class="form-control" rows="4"></textarea>
                                <div class="invalid-feedback">Silahkan isi</div>
                              </div>
                           </div> 
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Provinsi</label>
                              <select id="provinsi1" name="provinsi1" class="form-control select2bs4" style="width: 100%;">
                                <option value="">Silahkan Pilih</option>
                                @foreach($provinsi as $item)
                                <option value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                              </select>
                              <div class="invalid-feedback">Silahkan Pilih</div>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                              <label>Kecamatan</label>
                              <select id="kecamatan1" name="kecamatan1" class="form-control select2bs4" style="width: 100%;">
                                <option value="">Silahkan Pilih</option>
                              </select>
                              <div class="invalid-feedback">Silahkan Pilih</div>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kota/Kab</label>
                              <select id="kota1" name="kota1" class="form-control select2bs4" style="width: 100%;">
                                <option value="">Silahkan Pilih</option>
                              </select>
                              <div class="invalid-feedback">Silahkan Pilih</div>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                              <label>Kelurahan</label>
                              <select id="kelurahan1" name="kelurahan1" class="form-control select2bs4" style="width: 100%;">
                                <option value="">Silahkan Pilih</option>
                              </select>
                              <div class="invalid-feedback">Silahkan Pilih</div>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col -->
                        </div>
                        {{--
                        <p class="text-muted">
                           {{$data[0]['store_address']}}&nbsp;,
                        Provinsi &nbsp; {{$data[0]['provinsi1']}} ,
                        Kota / Kab &nbsp;{{$data[0]['kota1']}} ,
                        Kecamatan &nbsp; {{$data[0]['kecamatan1']}} ,
                        Kelurahan &nbsp; {{$data[0]['kelurahan1']}} 

                        </p>
                        --}}

                      </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <div class="card-body">
                        <strong><i class="fas fa-book mr-1"></i> File KTP</strong>

                        <p class="text-muted">
                         <a href="{{asset('public/upload_dokumen')}}/{{$data[0]['register_id']}}_ktp_{{$data[0]['file_ktp']}}" target="_blank">{{$data[0]['file_ktp']}}</a>
                        </p>

                        <hr>

                        <strong><i class="fas fa-book mr-1"></i> File KK</strong>

                        <p class="text-muted">
                         <a href="{{asset('public/upload_dokumen')}}/{{$data[0]['register_id']}}_kk_{{$data[0]['file_kk']}}" target="_blank">{{$data[0]['file_kk']}}</a>
                        </p>

                        <hr>

                        <strong><i class="fas fa-book mr-1"></i> File NPWP</strong>

                        <p class="text-muted">
                         <a href="{{asset('public/upload_dokumen')}}/{{$data[0]['register_id']}}_npwp_{{$data[0]['file_npwp']}}" target="_blank">{{$data[0]['file_npwp']}}</a>
                        </p>

                        <hr>

                        <strong><i class="fas fa-book mr-1"></i> File Lokasi Usaha</strong>

                        <p class="text-muted">
                         <a href="{{asset('public/upload_dokumen')}}/{{$data[0]['register_id']}}_lok_usaha_{{$data[0]['file_store_1']}}" target="_blank">{{$data[0]['file_store_1']}}</a>
                        </p>

                        <hr>

                        <strong><i class="fas fa-book mr-1"></i> File lainnya</strong>

                        <p class="text-muted">
                         {{$data[0]['store_name']}}
                        </p>

                        <hr>

                        <strong><i class="fas fa-book mr-1"></i> File Foto Selfie</strong>

                        <p class="text-muted">
                          <a href="{{asset('public/upload_dokumen')}}/{{$data[0]['register_id']}}_selfie_{{$data[0]['file_selfie_1']}}" target="_blank">{{$data[0]['file_selfie_1']}}</a>
                        </p>

                        <hr>

                        <strong><i class="fas fa-book mr-1"></i> File Foto Selfie Dengan KTP</strong>

                        <p class="text-muted">
                         <a href="{{asset('public/upload_dokumen')}}/{{$data[0]['register_id']}}_selfiektp_{{$data[0]['file_selfie_2']}}" target="_blank">{{$data[0]['file_selfie_2']}}</a>
                        </p>


                      </div>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </form>



        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
  


  function simpan() {
    if($('#notes').val()===''){
      $( "#notes" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#notes").removeClass( "is-invalid" );

    }
    if($('#status').val()===''){
      $( "#status" ).addClass( "is-invalid" );
      return false;
    }else{
      $("#status").removeClass( "is-invalid" );

    }



    var formData = document.getElementById("form-input");
    var objData = new FormData(formData);

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
     });

    $.ajax({
      type: 'POST',
      url: '{{ route('status-jawara') }}',
      data: objData,
      contentType: false,
      cache: false,
      processData: false,

      beforeSend: function () {
        $("#loading").css('display', 'block');
      },

      success: function (response) {
        console.log(response);
        var response=JSON.parse(response);
        $("#loading").css('display', 'none');

        console.log('===========');
        console.log(response['statusCode']);
        console.log(response.statusCode);
        console.log('===========');

        if(response.statusCode!=200){
          swal.fire("info",response.message,"info");
        }else{
         swal.fire({
                 title: "Info",
                 text: "Data Berhasil Disimpan",
                 type: "success",
                 background: 'white',
                 confirmButtonText: "Tutup",
                 closeOnConfirm: true
              }).then(function(result){
                  if (result.value) {
                      window.location.href = '{{ route('mitra-jawara') }}';
                  }
              });

        }

        
      }

    }).done(function (msg) {
      $("#loading").css('display', 'none');
    }).fail(function (response) {
      console.log(objData);
      $("#loading").css('display', 'none');
      swal.fire("error",'Terjadi Kesalahan',"error");
      // toastr.error("Terjadi Kesalahan");
    });
   

  }
  </script>
@stop