<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Schema;
use Auth;
use Form;
use Request;
use Excel;
use PDF;
use Session;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class MyFunc extends Model
{
	
    //
	public function __construct()
    {
        $this->middleware('auth');
        
    }
	
	public static function hello(){
		return 'hello World';
	}

	public static function printto($module,$judul,$fields,$perpage=20,$view=false,$arah='portrait',$kertas="a4"){		
		//$html = false;
		//$mode=='excel'
		//if(Request::has('print')){
		/*
			$mode=Request::input('print');
			if($mode=='excel') {
					$qry = $module->get();					
			} else {
					if($perpage==0) $qry = $module->get();
					else $qry = $module->paginate($perpage);
			}
		*/

			//set_time_limit(0);			
			//if($view==false) $view=['pdf'=>'layouts.pdf','excel'=>'layouts.excel'];			
			//switch($mode){
				//case 'pdf':$html = MyFunc_ade::pdf($tgl,$wkt,$qry,$judul,$fields,$view['pdf'],$arah,$kertas);break;
		//$html = MyFunc::excel($qry,$judul,$fields,$view['excel']);
			//}
			//set_time_limit(intval(30));
		//}
		return MyFunc::excel($module,$judul,$fields,$view['excel']);
	}


	public static function excel($module,$judul,$fields,$view='layouts.excel'){
		$title=$judul;
		$data=['module'=>$module,'title'=>$title,'fields'=>$fields,'view'=>$view];
		Excel::create($title, function($excel) use($data) {
			$excel->sheet('Table ', function($sheet) use($data) {				
				$sheet->loadView($data['view'],$data);
			});
		})->download('xlsx');
		 		//->store('xls',public_path('temp/excel'));
		

		/*
		$html = '<a data-dismiss="modal" class="text-center btn btn-success" href="#" onclick="window.location.href=\''.url('temp/excel').'/'.str_slug($title).'.xls\'">
					Download <br>
					<span class="fa fa-file-excel-o" aria-hidden="true"> 
						'.str_slug($title).'.xls
					</span>
				</a>';
		return $html;
		*/
	}
	public static function pdf($module, $judul, $fields, $view='layouts.pdf',$arah='lanscape',$kertas="f4"){	
	$html='';
	$title=$judul;
				
	$pdf = PDF::loadView($view,compact('module','title','fields'))->setPaper($kertas, $arah)->setWarnings(false);
	//$pdf = PDF::loadView($view, compact('module','title','fields'))->setPaper('A4')->stream();
	return $pdf->download($title.'.pdf');	
	
	}

}
?>