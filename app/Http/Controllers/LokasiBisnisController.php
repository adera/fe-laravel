<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\MyFunc;
use Maatwebsite\Excel\Facades\Excel;  
//use Excel;
use App\Exports\LokasiBisnisExport;

class LokasiBisnisController extends Controller
{
    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(Session::get("user") == false && empty(Session::get('user'))) {
                Redirect::to('view_login')->send();
            }
            return $next($request);
        });
    }
   
     public function index(Request $request)
    {
        $url = env('API_BASE_URL')."/lokasibisnis";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $param4=[];
            $param4['data']=$data['data'];

            return view('master.master')->nest('child', 'lokasi.index',$param4);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

    public function export_excel()
    {
        $url = env('API_BASE_URL')."/lokasibisnis";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $param4=[];
            $param4['data']=$data['data'];
            $fields=[];
            //return MyFunc::printto($data['data'],'Laporan CMC', $fields, 0,['excel'=>'bisnis.excel'],'landscape','a4');
            return Excel::download(new LokasiBisnisExport($data['data']), 'export-lokasi-bisnis.xlsx');
           

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

}

