<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use Illuminate\Support\Facades\Redirect;
class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function login1(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json'
        ];

        $data = array(
            'username'=> $request->input('username'),
            'password' => $request->input('password'),
        );

        $url = env('API_BASE_URL')."/auth/login";
        $urlmenu = env('API_BASE_URL')."/role-menu";
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            $arrsession=array(
                'token' => $data_result['data']['token'],
                'id' => $data_result['data']['id'],
                'first_name' => $data_result['data']['first_name'],
                'last_name' => $data_result['data']['last_name'],
                'role_id' => $data_result['data']['role_id'],
                'id_company' => $data_result['data']['id_company'],
                
            );
            session(['user' => $arrsession]);
            $headers1 = [
            'Authorization' => 'Bearer '. Session('user')['token']
            ];

            $result1 = $client->get($urlmenu,[
                RequestOptions::HEADERS => $headers1
            ]);

            $param1=[];
            $param1= (string) $result1->getBody();
            $data_result1 = json_decode($param1, true);
            
            session(['menu' => $data_result1['data']]);


            return json_encode($data_result);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }

    }

    public function view_login(){
        return view('login');
    }

    public function logout(){
        session()->flush();
        return redirect()->route('home');
    }
    
}

