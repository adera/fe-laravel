<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\MyFunc;
  


class HomeController extends Controller
{
    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(Session::get("user") == false && empty(Session::get('user'))) {
                Redirect::to('view_login')->send();
            }
            return $next($request);
        });
    }
   
     public function index(Request $request)
    {
        $url = env('API_BASE_URL')."/dashboard/dashboard-loca-register";
        $url1 = env('API_BASE_URL')."/dashboard/dashboard-loca-aktif";
        $url2 = env('API_BASE_URL')."/dashboard/dashboard-loca-transaksi";
        $url3 = env('API_BASE_URL')."/dashboard/dashboard-loca-layanan";

        $url4 = env('API_BASE_URL')."/dashboard/dashboard-bisnis";
        $url5 = env('API_BASE_URL')."/dashboard/dashboard-bisnis-lokasi";


        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers
            ]);
            $param1=[];
            $param1= (string) $result1->getBody();
            $data1 = json_decode($param1, true);

            $result2 = $client->get($url2,[
                RequestOptions::HEADERS => $headers
            ]);
            $param2=[];
            $param2= (string) $result2->getBody();
            $data2 = json_decode($param2, true);

            $result3 = $client->get($url3,[
                RequestOptions::HEADERS => $headers
            ]);
            $param3=[];
            $param3= (string) $result3->getBody();
            $data3 = json_decode($param3, true);

            $result4 = $client->get($url4,[
                RequestOptions::HEADERS => $headers
            ]);
            $param4=[];
            $param4= (string) $result4->getBody();
            $data4 = json_decode($param4, true);

            $result5 = $client->get($url5,[
                RequestOptions::HEADERS => $headers
            ]);
            $param5=[];
            $param5= (string) $result5->getBody();
            $data5 = json_decode($param5, true);





            $param6=[];
            $param6['data']=$data['data'];
            $param6['data1']=$data1['data'];
            $param6['data2']=$data2['data'];
            $param6['data3']=$data3['data'];
            $param6['data4']=$data4['data'];
            $param6['data5']=$data5['data'];

            return view('master.master')->nest('child', 'home',$param6);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

    public function kota(Request $request)
    {
       
        $url = env('API_BASE_URL')."/ref/kota/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers


            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data['data']);
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
    }

    public function kecamatan(Request $request)
    {
       
        $url = env('API_BASE_URL')."/ref/kecamatan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers


            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data['data']);
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
    }

    public function kelurahan(Request $request)
    {
       
        $url = env('API_BASE_URL')."/ref/kelurahan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers


            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data['data']);
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
    }

    public function detail_jawara(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/register-loca/".$request->get('id');

        $urlbisnis = env('API_BASE_URL')."/ref/bank";
        $urlprovinsi = env('API_BASE_URL')."/ref/provinsi";
        $param1=[];
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            return json_encode($data_result['data']);
        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }


}

