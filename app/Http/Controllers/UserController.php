<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\MyFunc;
  


class UserController extends Controller
{
    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(Session::get("user") == false && empty(Session::get('user'))) {
                Redirect::to('view_login')->send();
            }
            return $next($request);
        });
    }
   
     public function index(Request $request)
    {
        $url = env('API_BASE_URL')."/user";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $param4=[];
            $param4['data']=$data['data'];
            return view('master.master')->nest('child', 'user.index',$param4);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }


     public function edit(Request $request)
    {
        $url = env('API_BASE_URL')."/user/".$request->get('id');
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $param4=[];
            $param4['data']=$data['data'];
            return view('master.master')->nest('child', 'user.edit',$param4);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }


       public function update(Request $request)
    {
        $url = env('API_BASE_URL')."/user";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];
        $data = array(
            "id"=>$request->input('id'),
            "username"=>$request->input('username'),
            "email"=>$request->input('email'),
            "first_name"=>$request->input('first_name'),
            "last_name"=>$request->input('last_name'),
            "phone"=>$request->input('phone'),
            "referall_code"=>$request->input('referall_code')
            );
               
        
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }


       public function non_aktif(Request $request)
    {
        $url = env('API_BASE_URL')."/user/non-aktif/".$request->get('id');
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];  
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

      public function aktif(Request $request)
    {
        $url = env('API_BASE_URL')."/user/aktif/".$request->get('id');
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];  
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

    public function tambah(Request $request)
    {
        
        return view('master.master')->nest('child', 'user.add');
        
    }

    public function insertuser(Request $request){
        $url = env('API_BASE_URL')."/user";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];
        $data = array(
            "ip_address"=>$request->input('ip'),
            "username"=>$request->input('username'),
            "password"=>bcrypt('admin123'),
            "email"=>$request->input('email'),
            "first_name"=>$request->input('first_name'),
            "last_name"=>$request->input('last_name'),
            "phone"=>$request->input('phone'),
            "referall_code"=>$request->input('referall_code')
            );
               
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
    }

}

