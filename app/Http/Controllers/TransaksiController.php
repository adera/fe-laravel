<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\MyFunc;
use Maatwebsite\Excel\Facades\Excel;  
//use Excel;
use App\Exports\BisnisExport;

class TransaksiController extends Controller
{
    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(Session::get("user") == false && empty(Session::get('user'))) {
                Redirect::to('view_login')->send();
            }
            return $next($request);
        });
    }
   
     public function index(Request $request)
    {   $data1='';
        $param=[];
        if($request->get('tgl')){
            $url = "https://api.myloca.id/ref/transaksi";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        $data = array(
            "tgl"=>$request->get('tgl'),
            "tgl1"=>$request->get('tgl1')
        );
               
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);
            
            $param= (string) $result->getBody();
            $data1 = json_decode($param, true);
            $param['data1']=$data1;
            
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        }
        return view('master.master')->nest('child', 'transaksi.index',$param); 

        
        /*
        $url = "https://api.myloca.id/ref/transaksi";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        $data = array(
            "tgl"=>'2022-01-01',
            "tgl1"=>'2022-05-30'
        );
               
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data1 = json_decode($param, true);
           
            return json_encode($data);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        */

        
    }

    public function export_excel()
    {
        $url = env('API_BASE_URL')."/bisnis";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $param4=[];
            $param4['data']=$data['data'];
            $fields=[];
            //return MyFunc::printto($data['data'],'Laporan CMC', $fields, 0,['excel'=>'bisnis.excel'],'landscape','a4');
            return Excel::download(new BisnisExport($data['data']), 'export-bisnis.xlsx');
           

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

    public function transaksi_isat(Request $request)
    {
        $paramm=[];
        if($request->has('Filter')){
            $url = "http://localhost:8002/ref/transaksi-isat";
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. Session('user')['token']

            ];
            $date=explode("-",$request->input('reportrange'));
            $data = array(
                "grup"=>$request->input('grup'),
                "tgl"=>date('Y-m-d',strtotime($date[0])),
                "tgl1"=>date('Y-m-d',strtotime($date[1]))
            );
                   
            
            try{
                $result = $client->post($url,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data
                ]);
                $param=[];
                $param= (string) $result->getBody();
                $data1 = json_decode($param, true);
                $paramm['data']=$data1;
            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $paramm['data']=[];
            }
        }
        return view('master.master')->nest('child', 'transaksi.index1',$paramm);
        //return view('transaksi.index1'); 
    }

    public function filter_trx(Request $request)
    {
        $url = "http://localhost:8002/ref/transaksi-isat";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];
        $date=explode("-",$request->input('reportrange'));
        $data = array(
            "grup"=>$request->input('grup'),
            "tgl"=>date('Y-m-d',strtotime($date[0])),
            "tgl1"=>date('Y-m-d',strtotime($date[1]))
        );
               
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data1 = json_decode($param, true);
            return json_encode($data1);
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            
        }
    }

}

