<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use Illuminate\Support\Facades\Redirect;
class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(Session::get("user") == false && empty(Session::get('user'))) {
                Redirect::to('view_login')->send();
            }
            return $next($request);
        });
    }

    public function registerloca(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/register-loca";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
          
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'register.loca',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }

    }

     public function registerjawara(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/register-jawara";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'register.jawara',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }

    }

    public function detailloca(Request $request){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/register-loca/".$request->get('id');

        $urlbisnis = env('API_BASE_URL')."/ref/bisnis-type";
        $urlprovinsi = env('API_BASE_URL')."/ref/provinsi";
        $urlregister = env('API_BASE_URL')."/ref/type-register";
        $param1=[];
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);

            $result2 = $client->get($urlbisnis,[
                RequestOptions::HEADERS => $headers
            ]);

            $param2=[];
            $param2= (string) $result2->getBody();
            $data_result2 = json_decode($param2, true);

            $result3 = $client->get($urlprovinsi,[
                RequestOptions::HEADERS => $headers
            ]);

            $param3=[];
            $param3= (string) $result3->getBody();
            $data_result3 = json_decode($param3, true);


            $result4 = $client->get($urlregister,[
                RequestOptions::HEADERS => $headers
            ]);

            $param4=[];
            $param4= (string) $result4->getBody();
            $data_result4 = json_decode($param4, true);



            $param1['data']=$data_result['data'];
            $param1['bisnis']=$data_result2['data'];
            $param1['provinsi']=$data_result3['data'];
            $param1['register']=$data_result4['data'];

            return view('master.master')->nest('child', 'register.loca_detail',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
         
    }

    public function simpanloca(Request $request){
        $url = env('API_BASE_URL')."/register/";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
           'Authorization' => 'Bearer '. Session('user')['token']
        ];
        $data = array(
            "register_id"=>$request->input('idregis'),
            "business_name"=>$request->input('nama_bisnis'),
            "business_type_id"=>$request->input('bisnis'),
            "address"=>$request->input('alamat'),
            "province_id"=>$request->input('provinsi'),
            "regency_id"=>$request->input('kota'),
            "district_id"=>$request->input('kecamatan'),
            "village_id"=>$request->input('kelurahan'),
            "postical_code"=>$request->input('kodepos'),
            "type_register"=>$request->input('register'),
            );
               
        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);

            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }

    }

    public function detailjawara(Request $request){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/register-loca/".$request->get('id');

        $urlbisnis = env('API_BASE_URL')."/ref/bank";
        $urlprovinsi = env('API_BASE_URL')."/ref/provinsi";
        $param1=[];
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);

            $result2 = $client->get($urlbisnis,[
                RequestOptions::HEADERS => $headers
            ]);

            $param2=[];
            $param2= (string) $result2->getBody();
            $data_result2 = json_decode($param2, true);

            $result3 = $client->get($urlprovinsi,[
                RequestOptions::HEADERS => $headers
            ]);

            $param3=[];
            $param3= (string) $result3->getBody();
            $data_result3 = json_decode($param3, true);



            $param1['data']=$data_result['data'];
            $param1['bisnis']=$data_result2['data'];
            $param1['provinsi']=$data_result3['data'];

            return view('master.master')->nest('child', 'register.jawara_detail',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
         
    }

    public function simpanjawara(Request $request){
      $filenamektp='';
    if ($request->hasFile('ktp')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->ktp;
            $filenamektp = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_ktp_" . $filenamektp);
        }
        $filenamekk='';
    if ($request->hasFile('kk')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->kk;
            $filenamekk = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_kk_" . $filenamekk);
        } 
        $filenamenpwp='';
    if ($request->hasFile('npwp')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->npwp;
            $filenamenpwp = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_npwp_" . $filenamenpwp);
        }
        $filenamelokusaha='';
    if ($request->hasFile('lok_usaha')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->lok_usaha;
            $filenamelokusaha = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_lok_usaha_" . $filenamelokusaha);
        }  
    $filenameselfie='';
    if ($request->hasFile('selfie')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->selfie;
            $filenameselfie = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_selfie_" . $filenameselfie);
        }                
$filenameselfiektp='';
    if ($request->hasFile('selfiektp')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->selfiektp;
            $filenameselfiektp = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_selfiektp_" . $filenameselfiektp);
        }
    $filenamelain ='';    
    if ($request->hasFile('lain')) {

            $destination_path = public_path('upload_dokumen');
            $files = $request->lain;
            $filenamelain = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('idregis') . "_lain_" . $filenamelain);
        }



        $url = env('API_BASE_URL')."/register/jawara";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
           'Authorization' => 'Bearer '. Session('user')['token']
        ];
        $data = array(
            "register_id"=>$request->input('idregis'),
            "address"=>$request->input('alamat'),
            "rt"=>$request->input('rt'),
            "rw"=>$request->input('rw'),
            "province_id"=>$request->input('provinsi'),
            "regency_id"=>$request->input('kota'),
            "district_id"=>$request->input('kecamatan'),
            "village_id"=>$request->input('kelurahan'),
            "postical_code"=>$request->input('kodepos'),
            "store_name"=>$request->input('nama_usaha'),
            "store_address"=>$request->input('alamat1'),
            "store_province_id"=>$request->input('provinsi1'),
            "store_regency_id"=>$request->input('kota1'),
            "store_district_id"=>$request->input('kecamatan1'),
            "store_village_id"=>$request->input('kelurahan1'),
            "file_ktp"=>$filenamektp,
            "file_kk"=>$filenamekk,
            "file_npwp"=>$filenamenpwp,
            "file_store_1"=>$filenamelokusaha,
            "file_store_2"=>$filenamelain,
            "file_selfie_1"=>$filenameselfie,
            "file_selfie_2"=>$filenameselfiektp,
            "bank_account"=>$request->input('bank'),
            "bank_account_number"=>$request->input('norek'),
            "bank_account_name"=>$request->input('nama_pemilik'),
            "self_assesment_loan"=>$request->input('pinjaman'),

            );
               
        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);

            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }        

    }


    public function mitralocaumkm(Request $request){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/mitra-loca/1";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_loca.umkm',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }

    public function mitralocacommercial(Request $request){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/mitra-loca/2";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_loca.commercial',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }

    public function detailmitraloca(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/detail-mitra-loca/".$request->get('id');
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_loca.detail',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }

    public function mitrajawara(Request $request){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/mitra-japang/all";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_japang.index',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
        

    }

    public function detailmitrajapang(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/detail-mitra-japang/".$request->get('id');
        $urlstatus = env('API_BASE_URL')."/ref/status-japang";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
             $result2 = $client->get($urlstatus,[
                RequestOptions::HEADERS => $headers
            ]);

            $param2=[];
            $param2= (string) $result2->getBody();
            $data_result2 = json_decode($param2, true);


            $param1['status']=$data_result2['data'];
             $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_japang.detail',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }

    public function editmitrajapang(Request $request)
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/detail-mitra-japang/".$request->get('id');
      
        
        $urlbisnis = env('API_BASE_URL')."/ref/bank";
        $urlprovinsi = env('API_BASE_URL')."/ref/provinsi";

        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $result2 = $client->get($urlbisnis,[
                RequestOptions::HEADERS => $headers
            ]);

            $param2=[];
            $param2= (string) $result2->getBody();
            $data_result2 = json_decode($param2, true);

            $result3 = $client->get($urlprovinsi,[
                RequestOptions::HEADERS => $headers
            ]);

            $param3=[];
            $param3= (string) $result3->getBody();
            $data_result3 = json_decode($param3, true);


             $param1['provinsi']=$data_result2['data'];
            $param1['bisnis']=$data_result2['data'];
             $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_japang.edit',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }

    public function mitrajawaraaktif(Request $request){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/mitra-japang/11";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_japang.aktif',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }

    }
    public function mitrajawaranonaktif(Request $request){
         $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $url = env('API_BASE_URL')."/register/mitra-japang/95";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);

            $param=[];
            $param1=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            $param1['data']=$data_result['data'];
            return view('master.master')->nest('child', 'mitra_japang.non_aktif',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }

    }

    public function statusjawara(Request $request)
    {
        $url = env('API_BASE_URL')."/register/status-jawara";
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
           'Authorization' => 'Bearer '. Session('user')['token']
        ];
        $data = array(
            "register_id"=>$request->input('idregis'),
            "notes"=>$request->input('notes'),
            "register_status_id"=>$request->input('status'),
            );
               
        try{
            
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);

            return json_encode($data1);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
        }        
    }
    



}

