<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\MyFunc;
  


class CompanyController extends Controller
{
    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(Session::get("user") == false && empty(Session::get('user'))) {
                Redirect::to('view_login')->send();
            }
            return $next($request);
        });
    }
   
     public function index(Request $request)
    {
        $url = env('API_BASE_URL')."/company";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $param4=[];
            $param4['data']=$data['data'];
            return view('master.master')->nest('child', 'company.index',$param4);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
        
    }

    public function tambah(){
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']
        ];

        $urlprovinsi = env('API_BASE_URL')."/ref/provinsi";
        $urlcompany = env('API_BASE_URL')."/ref/type-company";
        $param1=[];
        try{
            $result2 = $client->get($urlcompany,[
                RequestOptions::HEADERS => $headers
            ]);

            $param2=[];
            $param2= (string) $result2->getBody();
            $data_result2 = json_decode($param2, true);

            $result3 = $client->get($urlprovinsi,[
                RequestOptions::HEADERS => $headers
            ]);

            $param3=[];
            $param3= (string) $result3->getBody();
            $data_result3 = json_decode($param3, true);

            $param1['company']=$data_result2['data'];
            $param1['provinsi']=$data_result3['data'];

            return view('master.master')->nest('child', 'company.add',$param1);

        }catch (BadResponseException $e){

            $response = json_decode($e->getResponse()->getBody());
            return json_encode($response);
            
        }
    }

    public function simpan(Request $request)
    {
        $url = env('API_BASE_URL')."/company";
     
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. Session('user')['token']

        ];

        $data = array(
            
            "name"=>$request->input('nama'),
            "address"=>$request->input('alamat'),
            "province_id"=>$request->input('provinsi'),
            "regency_id"=>$request->input('kota'),
            "district_id"=>$request->input('kecamatan'),
            "village_id"=>$request->input('kelurahan'),
            "postical_code"=>$request->input('kodepos'),
            "phone"=>$request->input('phone'),
            "company_type"=>$request->input('tipe'), 

            "username"=>$request->input('username'),
            "password"=>bcrypt('admin123'),
            "email"=>$request->input('email'),
            "first_name"=>$request->input('first_name'),
            "last_name"=>$request->input('last_name'),
            "phone1"=>$request->input('phone1'),
            "ip"=>$request->input('ip'),
            

            );
               
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data
            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            return json_encode($data);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
        }
    }



}

